# jDev GameSRV (python)
The second version of our game server, now written in Python using FastAPI.

## Usage
To launch the server process simply run the following command:
```sh
python src/app.py
```

## Tedd'or
The Tedd'or servers run here, everything runs on a single process. It's not optimal, but it's easier to work with.

## Interman
Interman is our CLI for managing the server process from the shell.

> Please keep in mind Interman needs to be run as root for security reasons, this can be disabled by creating a `.nosudo` file with root ownership.

```sh
./interman.py help
```

