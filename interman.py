#!/usr/bin/python3

import sys, os, requests, urllib3
import urllib.parse
from pprint import pprint as pprinter
urllib3.disable_warnings()

def pprint(x):
    if 'code' in x: print(f"[{x['code']}] {x['message']}")
    else: pprinter(x)

def run_update():
    pprint(requests.get("https://127.0.0.1/api/interman?action=update", verify=False).json())
def run_reboot():
    pprint(requests.get("https://127.0.0.1/api/interman?action=reboot", verify=False).json())
def run_setval(uid, k, v):
    pprint(requests.get(f"https://127.0.0.1/api/interman?action=set_{k}&uid={uid}&value={v}", verify=False).json())
def run_getprofile(uid):
    pprint(requests.get(f"https://127.0.0.1/api/interman?action=get_profile&uid={uid}", verify=False).json())
def run_teddor(subc, args):
    if subc == 'broadcast':
        pprint(requests.get(f"https://127.0.0.1/api/interman?action=ted_broadcast&value={urllib.parse.quote(' '.join(args))}", verify=False).json())
    else: print(f"Invalid Tedd'or subcommand: {subc}")

def run_pushone(uid, value):
    pprint(requests.get(f"https://127.0.0.1/api/interman?action=push_one&uid={uid}&value={value}", verify=False).json())

def run_pushall(value):
    pprint(requests.get(f"https://127.0.0.1/api/interman?action=push_all&value={value}", verify=False).json())

def run_help():
    print("Usage: interman <command> [<options>]\n")
    print("  update")
    print("  restart")
    print("  reboot")
    print("  admin <uid> <value>")
    print("  premium <uid> <value>")
    print("  bp <uid> <value>")
    print("  profile <uid>")
    print("  pushone <uid> <value>")
    print("  pushall <value>")
    print("  ted broadcast <message>")

def main(args):
    if len(args) < 1: return run_help()
    if args[0] == 'update':
        return run_update()
    elif args[0] == 'reboot':
        return run_reboot()
    elif args[0] == 'restart':
        return os.system('systemctl restart gamesrv')
    elif args[0] == 'admin':
        return run_setval(args[1], 'admin', args[2])
    elif args[0] == 'premium':
        return run_setval(args[1], 'premium', args[2])
    elif args[0] == 'bp':
        return run_setval(args[1], 'bp', args[2])
    elif args[0] == 'profile':
        return run_getprofile(args[1])
    elif args[0] == 'ted':
        return run_teddor(args[1], args[2:])
    elif args[0] == 'pushone':
        return run_pushone(args[1], ' '.join(args[2:]))
    elif args[0] == 'pushall':
        return run_pushall(' '.join(args[1:]))

    return run_help()

if __name__ == '__main__':
    # removed root requirements, not needed as the server must be secured anyways
    main(sys.argv[1:])
