import bson, os
cc = {}

class JsonDbCollection:
    def __init__(self, name):
        self.name = name

    def get(self, name):
        try:
            return cc[self.name][name]
        except KeyError: return None

    def has(self, name):
        return name in cc[self.name]

    def keys(self):
        return list(cc[self.name].keys())

    def set(self, name, value):
        cc[self.name][name] = value
        return self

    def sget(self, name) -> dict:
        v = self.get(name)
        self.close()
        return v

    def update(self, name, value):
        cc[self.name][name].update(value)
        return self
    
    def store(self):
        with open(f"jsondb/{self.name}.bin", 'wb') as f:
            f.write(bson.dumps(cc[self.name]))

    def close(self):
        self.store()

def collection(name):
    if name in cc: return JsonDbCollection(name)
    if not os.path.exists('jsondb/'): os.mkdir('jsondb/')
    dat = None
    fp = f"jsondb/{name}.bin"
    if not os.path.exists(fp):
        with open(fp, 'wb') as f: f.write(bson.dumps({}))
    with open(fp, 'rb') as f:
        dat = bson.loads(f.read())
    cc[name] = dat
    return JsonDbCollection(name)
