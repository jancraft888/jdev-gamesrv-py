from fastapi import WebSocketDisconnect
from starlette.websockets import WebSocket
import utils, jsondb, secrets

class TeddorInstance:
    def __init__(self, network, instance_id):
        self.network = network
        self.db = network.db
        self.instance_id = instance_id
        self.__init_resources()

        self.connections = {}
        self.players = {}

    def __init_resources(self): pass

    async def add_player(self, uid, game_data, websocket):
        self.players[uid] = game_data
        self.connections[uid] = websocket
        await self.broadcast({ 'packet': 'server_addclient', 'uid': uid })

    async def remove_player(self, uid):
        del self.players[uid]
        del self.connections[uid]
        await self.broadcast({ 'packet': 'server_removeclient', 'uid': uid })

    async def broadcast(self, message):
        for connection in self.connections:
            await connection.send_json(message)

    async def onpacket(self, uid, packet):
        print(f"[TD-{self.instance_id}] (#{uid}) -> {packet}")

class TeddorNetwork:
    instances = {}

    def __init__(self):
        self.active_connections = []
        self.__init_db()

    def __init_db(self):
        self.db = jsondb.collection('teddormainnet')

    def create_instance(self, instance_id): return TeddorInstance(self, instance_id)

    async def connect(self, websocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket):
        self.active_connections.remove(websocket)

    async def close_and_disconnect(self, websocket):
        self.disconnect(websocket)
        await websocket.close()

    async def broadcast(self, message):
        for connection in self.active_connections:
            await connection.send_json(message)

    def ensure_game_data(self, profile):
        if self.db.has(profile['uid']): return self.db.get(profile['uid'])
        gamedata = {}
        self.db.set(profile['uid'], gamedata)
        return gamedata

    def ensure_instance(self, instance_id):
        if instance_id in self.instances: return self.instances[instance_id]
        i = self.create_instance(instance_id)
        self.instances[instance_id] = i
        return i

    async def entry(self, websocket: WebSocket):
        await self.connect(websocket)
        try:
            handshake = await websocket.receive_json()
            if handshake['token'] is None: return await self.close_and_disconnect(websocket)
            token = utils.vftok(handshake['token'])
            if token is None: return await self.close_and_disconnect(websocket)
            profile_data = jsondb.collection('accounts').get(token['sub'])
            game_data = self.ensure_game_data(profile_data)
            instance_id = handshake['instance'] or secrets.token_hex(4)
            instance = self.ensure_instance(instance_id)
            await websocket.send_json({ 'packet': 'server_info', 'instance_id': instance_id, 'game_data': game_data })
            await instance.add_player(token['sub'], game_data, websocket)

            while True:
                await instance.onpacket(token['sub'], await websocket.receive_json())
        except WebSocketDisconnect:
            self.disconnect(websocket)
