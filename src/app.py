from dotenv import load_dotenv
from typing import Union
load_dotenv()

from fastapi import FastAPI, WebSocket, Response, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.middleware.httpsredirect import HTTPSRedirectMiddleware
from starlette.middleware.cors import CORSMiddleware
from fastapi.responses import PlainTextResponse
import urllib.parse
import uvicorn, os, sys, time, json, pywebpush
import utils, teddor, jsondb

DEBUG = False
teddor_mainnet = teddor.TeddorNetwork()

# npm i -g web-push
# web-push generate-vapid-keys --json
VAPID_KEYS = json.loads(os.environ.get('VAPID_KEYS'))

app = FastAPI()
app.add_middleware(HTTPSRedirectMiddleware)
app.add_middleware(CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],)

@app.get("/", response_class=PlainTextResponse)
def server_info():
    return "running jDev GameSRV v2.1"

@app.get("/api/accesstoken")
def api_accesstoken(request: Request, response: Response):
    tok = utils.validate_idtoken(request.headers['Authorization'].split(' ')[1])
    if tok is None:
        response.status_code = 401
        return { 'code': 401, 'message': 'Unauthorized' }

    token = utils.mktok(tok)
    accounts = jsondb.collection('accounts')
    if not accounts.has(tok['sub']):
        accounts.set(tok['sub'], { 'username': tok['name'], 'uid': tok['sub'], 'picture': tok['picture'], 'email': tok['email'], 'premium': False, 'admin': False, 'bp_ending': 0, 'created_at': int(time.time()) }).close()

    return { 'token': token }

@app.get("/api/profile")
async def api_profile(request: Request, response: Response) -> dict:
    tok = await utils.auth_middleware(request)
    if tok is None: response.status_code = 401; return { 'code': 401, 'message': 'Invalid or expired token' }

    return jsondb.collection('accounts').get(tok['uid'])

@app.get("/api/updateprofile")
async def api_updateprofile(request: Request, response: Response, username: Union[str, None] = None, email: Union[str, None] = None) -> dict:
    tok = await utils.auth_middleware(request)
    if tok is None: response.status_code = 401; return { 'code': 401, 'message': 'Invalid or expired token' }

    accounts = jsondb.collection('accounts')
    if username is not None:
        accounts.update(tok['uid'], { 'username': username })
    if email is not None:
        accounts.update(tok['uid'], { 'email': email })
    accounts.store()

    return accounts.get(tok['uid'])

@app.get("/api/viewprofile")
async def api_viewprofile(request: Request, uid: str, response: Response) -> dict:
    tok = await utils.auth_middleware(request)
    if tok is None: response.status_code = 401; return { 'code': 401, 'message': 'Invalid or expired token' }

    user = jsondb.collection('accounts').sget(uid)
    return { 'username': user['username'], 'picture': user['picture'], 'premium': user['premium'], 'admin': user['admin'], 'bp_active': user['bp_ending'] > time.time() }

@app.get('/push/vapidPublicKey', response_class=PlainTextResponse)
def push_vapidpublickey() -> str:
    return VAPID_KEYS['publicKey']

@app.post('/push/register')
async def push_register(request: Request) -> dict:
    tok = await utils.auth_middleware(request)
    if tok is None: response.status_code = 401; return { 'code': 401, 'message': 'Invalid or expired token' }
    sub = (await request.json())['subscription']
    pushsubs = jsondb.collection('pushsubs')
    pushsubs.set(tok['uid'], sub)
    pushsubs.close()
    return { 'code': 200, 'message': 'OK', 'subscription': sub }

@app.post("/api/webhook")
async def api_webhook(request: Request, response: Response) -> dict:
    if request.headers.get('X-Gitlab-Token') != os.environ.get('GAME_SRV_WEBHOOKTOKEN'): response.status_code = 401; return { 'code': 401, 'message': 'Invalid Webhook Token' }
    print("[!] New Gitlab commits. Clone and rebuild...")
    return { 'code': 200, 'message': await utils.update_procedure() }

@app.websocket("/teddor/mainnet")
async def teddor_mainnet_ws(websocket: WebSocket):
    await teddor_mainnet.entry(websocket)

def send_push_to_one(uid, data, pushsubs=None):
    cl = False
    if pushsubs is None: cl = True; pushsubs = jsondb.collection('pushsubs')
    subinf = pushsubs.get(uid)
    if cl: pushsubs.close()

    return pywebpush.webpush(
        subscription_info=subinf,
        data=data,
        vapid_private_key=VAPID_KEYS['privateKey'],
        vapid_claims={'sub':'mailto:jancraft.dev@gmail.com'},
    )

def send_push_to_all(data):
    pushsubs = jsondb.collection('pushsubs')
    keys = pushsubs.keys()

    for key in keys:
        try:
            send_push_to_one(key, data, pushsubs=pushsubs)
        except: pass

    pushsubs.close()

@app.get("/api/interman")
async def api_interman(request: Request, response: Response):
    if request.client.host != '127.0.0.1': response.status_code = 401; return { 'code': 401, 'message': 'Unauthorized' }
    act = request.query_params.get('action')
    done = False
    accounts = jsondb.collection('accounts')
    if act == 'set_admin':
        done = True
        if request.query_params.get('value') == '1': accounts.update(request.query_params.get('uid'), { 'admin': True })
        else: accounts.update(request.query_params.get('uid'), { 'admin': False })
    elif act == 'set_premium':
        done = True
        if request.query_params.get('value') == '1': accounts.update(request.query_params.get('uid'), { 'premium': True })
        else: accounts.update(request.query_params.get('uid'), { 'premium': False })
    elif act == 'set_bp':
        done = True
        accounts.update(request.query_params.get('uid'), { 'bp_ending': int(request.query_params.get('value')) })
    elif act == 'get_profile':
        return accounts.get(request.query_params.get('uid'))
    elif act == 'ted_broadcast':
        done = True
        await teddor_mainnet.broadcast({ 'packet': 'server_message', 'msg': urllib.parse.unquote(request.query_params.get('value')) })
    elif act == 'update':
        return { 'code': 200, 'message': await utils.update_procedure() }
    elif act == 'reboot':
        os.system('sudo shutdown -r +5')
        return { 'code': 200, 'message': 'OK. Reboot scheduled for +5 minutes.' }
    elif act == 'push_all':
        send_push_to_all(urllib.parse.unquote(request.query_params.get('value')))
        return { 'code': 200, 'message': 'OK' }
    elif act == 'push_one':
        msg = send_push_to_one(request.query_params.get('uid'), urllib.parse.unquote(request.query_params.get('value')))
        return { 'code': 200, 'message': msg }

    if not done: response.status_code = 404; return { 'code': 404, 'message': 'Not Found' }
    accounts.store()
    return { 'code': 200, 'message': 'OK' }

if __name__ == '__main__':
    DEBUG = '--dev' in (' '.join(sys.argv[1:]))
    uvicorn.run("app:app", port=int(os.environ.get('GAME_SRV_PORT', 443)), host="0.0.0.0", ssl_keyfile=os.environ.get('GAME_SRV_KEY'), ssl_certfile=os.environ.get('GAME_SRV_CERT'), reload=DEBUG)
