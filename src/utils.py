import jwt, requests, os, subprocess
import urllib.parse as urlpa
from cryptography.x509 import load_pem_x509_certificate

JWT_SECRET = os.environ.get('GAME_SRV_JWTSECRET')

def validate_idtoken(idtoken) -> dict:
    target_audience = 'jdev-es'
    certificate_url = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com'

    response = requests.get(certificate_url)
    certs = response.json()

    value = None
    try:
        kid = jwt.get_unverified_header(idtoken)['kid']
        cert = load_pem_x509_certificate(certs[kid].encode('ascii'))
        key = cert.public_key()
        value = jwt.decode(idtoken, key, algorithms=['RS256'], audience=target_audience) # type: ignore
    except Exception as e: print(e)

    return value # type: ignore

def urladdp(url, params):
    r = urlpa.urlparse(url)
    rp = urlpa.parse_qs(r.query)
    for k, v in params.items(): rp[k] = v
    return r._replace(query=urlpa.urlencode(rp)).geturl()

def mktok(firebase):
    return jwt.encode({ 'uid': firebase['sub'], 'pic': firebase['picture'], 'name': firebase['name'] }, JWT_SECRET, algorithm="HS256") # type: ignore

def vftok(token):
    try:
        return jwt.decode(token, JWT_SECRET, algorithms=['HS256']) # type: ignore
    except jwt.DecodeError or jwt.ExpiredSignatureError or jwt.InvalidSignatureError: return None

async def auth_middleware(req):
    auth = req.headers['Authorization'].split(' ')
    if auth[0].lower() != 'bearer': return None
    return vftok(auth[1])

async def update_procedure():
    process = subprocess.Popen(["git", "pull"], stdout=subprocess.PIPE)
    output = process.communicate()[0]
    return output.decode('utf-8')
